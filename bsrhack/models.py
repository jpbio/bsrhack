from django.db.models import Model,CharField,URLField
from django.db.models import DateTimeField,NullBooleanField,DateTimeField,IntegerField

class Practice(Model):
    company = CharField(max_length = 255)
    stage = CharField(max_length = 10)
    image = URLField(max_length = 255)
    practice = CharField(max_length = 255)
    tweeter = CharField(max_length = 30)
    created_at = DateTimeField(auto_now_add=True, null=True)
    consumer_yes = IntegerField(default='0')
    consumer_no = IntegerField(default='0')
    consumer_dn = IntegerField(default='0')
    maker_yes = IntegerField(default='0')
    maker_no = IntegerField(default='0')
    maker_dn = IntegerField(default='0')
    score = IntegerField(default='0')
    category = CharField(max_length = 255, null=True, blank=True)
