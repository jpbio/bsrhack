from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'bsrhack.views.home', name='home'),
    url(r'^results/$', 'bsrhack.views.results', name='results'),
    url(r'^(?P<category>[a-zA-Z ]+)/$', 'bsrhack.views.home', name='home'),
    
    url(r'^get_twitter/$', 'bsrhack.views.get_twitter', name='get_twitter'),

    url(r'^vote_up/$', 'bsrhack.views.vote_up', name='vote_up'),
    url(r'^vote_up/(?P<category>[a-zA-Z ]+)/$', 'bsrhack.views.vote_up', name='vote_up'),
    

    url(r'^vote_down/(?P<category>[a-zA-Z ]+)/$', 'bsrhack.views.vote_down', name='vote_down'),
    url(r'^vote_down/$', 'bsrhack.views.vote_down', name='vote_down'),

    # url(r'^add_practice/(?P<category>\w+)/$', 'bsrhack.views.add_practice', name='add_practice'),
    # url(r'^bsrhack/', include('bsrhack.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
