from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from bsrhack import models
from TwitterSearch import *
from helpers import company_categories

def home( request , category = 'food' ):
    # if category == 'uncategorized':
        # practices = models.Practice.objects.filter(category__isnull=True).order_by('-score')
    # else:
        # practices = models.Practice.objects.filter(category=category).order_by('-score')
    practices = models.Practice.objects.all().order_by('-score')
    categories = ['apparel', 'beer', 'coffee', 'food']
    return render( request, 'index.html',
                    {
                    'practices': practices,
                    'categories': categories,
                    'category': category,
                    } )


def vote_up(request, category = 'food'):
    if request.POST:
        voter = request.POST['voter']
        practice_id = request.POST['practice_id']
        practice = models.Practice.objects.get(pk = practice_id)
        
        if voter == 'consumer':
            practice.consumer_yes += 1
        else:
            practice.maker_yes += 1
        practice.score += 1
        practice.save()
    return HttpResponseRedirect(reverse('home', args=(category,)))

def vote_down(request, category = 'food'):
    if request.POST:
        practice_id = request.POST['practice_id']
        practice = models.Practice.objects.get(pk = practice_id)
        voter = request.POST['voter']
        if voter == 'consumer':
            practice.consumer_no += 1
        else:
            practice.maker_no += 1
        practice.score -= 1
        practice.save()
    return HttpResponseRedirect(reverse('home', args=(category,)))

def results(request):
    practices = models.Practice.objects.all().order_by('-score')
    return render( request, 'results.html',
                    {
                    'practices': practices,
                    } )

def get_twitter(request):
    print "Running"
    tso = TwitterSearchOrder()
    tso.setKeywords(['%23productbio'])
    tso.setCount(1)

    ts = TwitterSearch(
            consumer_key = 'zcaern5k6Y0E6JXqFdfuA',
            consumer_secret = 'CfUHR0JemC8VWnbuutyRk7WcAJEegW1Q4Sj2mwOS7o',
            access_token = '1967390456-eP6zCxqJvusvPATAtVVJdzx40cgdP7hzEt2M0Rs',
            access_token_secret = 'SnmZ6MYEOmQqRSVCoO6Gt1eXYXLq0FDgwurg2rEBFuqoj'
        )
    attrs = vars(tso)
    print ', '.join("%s: %s" % item for item in attrs.items())
    #del ts.exceptions[400]
    ts.authenticate()
    for tweet in ts.searchTweetsIterable(tso):
        print tweet
        # print('@%s tweeted: %s' % (tweet['user']['screen_name'], tweet['text']))
        text = tweet['text']
        productbio_loc = text.find('productbio')
        text = text[:productbio_loc-1] + text[productbio_loc + 11:]
        if "http://t.co" in text:
            end = text.find("http://t.co")
            text = text[:end]
        for hashtag in tweet['entities']['hashtags']:
            if hashtag['text'] != 'productbio':
                company_name = hashtag['text']
        try:
            image_url = tweet['entities']['media'][0]['media_url']
        except:
            image_url = ""
        pr = models.Practice.objects.get_or_create(
            practice = text,
            tweeter = tweet['user']['screen_name'],
            image = image_url,
            company = company_name,
        )
    return HttpResponseRedirect(reverse('home'))

def add_practice(request):
    """ not used """
    pr = models.Practice.objects.create(
        practice = "Some Practice",
        tweeter = "Some Tweeter",
        image = "https://pbs.twimg.com/media/BYiaGfnCUAE6Pq2.jpg",
        company = "honesttea"
        )
    return HttpResponseRedirect(reverse('home'))